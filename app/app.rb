require "sinatra/base"
require_relative "./models"

module BeehiveInc
  class App < Sinatra::Base

    set :public_folder, settings.root + '/../public'

    get '/' do
      @pollens = Pollen.all
    	@total_sugar = Harvest.total_sugar_harvested
      @total_harvest = Harvest.total_miligrams_harvested
    	@max_sugar = Pollen.all.max_by { |pollen| pollen.sugar_harvested }
      @max_harvest = Pollen.all.max_by { |pollen| pollen.miligrams_harvested}
      @minmax_days = Harvest.days_with_miligrams_harvested.minmax_by { |(_, harvest)| harvest }
      @minmax_bees = Harvest.effects_of_bees.minmax_by { |(_, eff)| eff}
      erb :index
    end

    helpers do
      def round(value, ord=0)
        value.round(0)
      end
    end
  end
end
