require "csv"
require "date"

class Array
	def sum
		self.inject(0){ |sum, obj| sum + obj.miligrams_harvested }
	end

	def avg
		self.sum.to_f / self.size unless self.size == 0
	end
end


class Pollen

	attr_reader :id, :name, :sugar_per_mg

	@@ind_hash ||= {}

	def initialize(args)
		@id = args[:id]
		@name = args[:name]
		@sugar_per_mg = args[:sugar_per_mg]
	end

	def self.load_data(filename=nil)
		filename ||= File.expand_path('pollens.csv', 'data')
		raise LoadError unless File.exist?(filename)
		CSV.foreach(filename, headers: true, header_converters: :symbol, converters: :all) do |row|
			pollen = Pollen.new row
			@@ind_hash[pollen.id] = pollen
		end
	end

	def self.find(id)
		@@ind_hash[id]
	end

	def self.all
		@@ind_hash.values
	end

	def harvests
		Harvest.select_pollen(self)
	end

	def miligrams_harvested
		harvests.sum
	end

	def sugar_harvested
		miligrams_harvested * sugar_per_mg
	end

	def to_s
		name
	end

end

class Harvest

	CSV::Converters[:str2day] = lambda{|s|
	  begin
	    Date.parse(s).to_date
	  rescue ArgumentError
	    s
	  end
	}

	attr_reader :bee_id, :day, :miligrams_harvested

	@@arr ||= []


	def initialize(args)
		@bee_id = args[:bee_id]
		@day = args[:day]
		@pollen_id = args[:pollen_id]
		@miligrams_harvested = args[:miligrams_harvested]
	end

	def self.get(ind)
		@@arr[ind]
	end

	def self.load_data(filename=nil)
		Pollen.load_data
		filename ||= File.expand_path('harvest.csv', 'data')
		raise LoadError.new(filename) unless File.exist?(filename)
		CSV.foreach(filename, headers: true, header_converters: :symbol, converters: [:all, :str2day]) do |row|
			@@arr << Harvest.new(row)
		end
	end

	def pollen
		Pollen.find(@pollen_id)
	end

	def self.select_pollen(pollen)
		@@arr.select{ |obj| obj.pollen == pollen }
	end

	def self.days_with_miligrams_harvested
		@@arr.group_by{ |harvest| harvest.day }.map{ |(day, harvests)| [day, harvests.sum]}
	end

	def self.effects_of_bees
		@@arr.group_by{ |harvest| harvest.bee_id}.map { |(bee_id, harvests)| [bee_id, harvests.avg] }
	end

	def self.total_miligrams_harvested
		@@arr.sum
	end

	def self.total_sugar_harvested
		@@arr.inject(0) {|sum, harvest| sum + harvest.miligrams_harvested * harvest.pollen.sugar_per_mg }
	end

end

Harvest.load_data
