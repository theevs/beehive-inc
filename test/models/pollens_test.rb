require "minitest/autorun"
require_relative('../../app/models')

class TestingPollens < Minitest::Test
	def setup
		Pollen.load_data
    @pollen = Pollen.find(Random.new.rand(1..5))
	end

  def test_id_type
    assert_instance_of Fixnum, @pollen.id
  end

  def test_name_type
    assert_instance_of String, @pollen.name
  end

  def test_sugar_per_mg
    assert_instance_of Fixnum, @pollen.sugar_per_mg
  end

  def test_size_pollens
    assert_equal 5, Pollen.all.size
  end

end
